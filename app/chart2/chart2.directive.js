(function (angular) {
    'use strict';
    
    var barChart = function(d3Service) {
        return{
        restrict: "E",
        replace: false,
        scope: {data: '=chartData'},
        link: function (scope, element, attrs)
        {
            d3Service.d3().then(function(d3) {
               
            var drawBarChart = function(){
                var chart = d3.select(element[0]);
                    chart.append("div").attr("class", "chart")
                    .selectAll('div')
                    .data(scope.data).enter().append("div")
                    .transition().ease("elastic")
                    .style("width", function(d) { return d + "%"; })
                    .text(function(d) { return d + "%"; });
            };
            
            drawBarChart();
            
            });
        }
    };
};

angular.module('myApp.chart2').directive('barChart', barChart)
    
})(angular);