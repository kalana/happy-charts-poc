(function (angular) {
    
    'use strict';

    var chart2Controller = function ($scope, $interval){
        $scope.chartData = [10,20,30,40,60, 80, 20, 50];
    };
    
    chart2Controller.inject = ['$scope','$interval'];
    
    angular.module('myApp.chart2').controller('Chart2Controller', chart2Controller);
    
})(angular);