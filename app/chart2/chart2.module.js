'use strict';

angular.module('myApp.chart2', ['ngRoute', 'd3'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/chart2', {
    templateUrl: 'chart2/chart2.html',
    controller: 'Chart2Controller',
    controllerAs: 'Chart2Controller'
  });
}])