'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.view3',
  'myApp.chart1',
  'myApp.chart2',
  'myApp.chart3',
  'myApp.c3simple',
  'myApp.chart4',
  'myApp.version',  
  'd3',
  //c3
  'gridshore.c3js.chart',
  'myApp.services'
  /*'ngMaterial',
  'ui.router',
  'gridshore.c3js.bar',
  'gridshore.c3js.line',
  'gridshore.c3js.pie',
  'gridshore.c3js.callback',
  'gridshore.c3js.donut',
  'gridshore.c3js.config',
  'gridshore.c3js.gauge',
  'gridshore.c3js.dynamic'*/
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/view1'});
}]);