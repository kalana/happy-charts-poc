'use strict';

angular.module('myApp.chart4', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/chart4', {
    templateUrl: 'chart4/chart4.html',
    controller: 'Chart4Controller',
    controllerAs: 'Chart4Controller'
  });
}])