(function(angular){
    'use strict';
    
    var chart4Controller = function ($scope, dataService, $interval) {
        
        //Chart 2
        $scope.datapoints=[{"x":10,"top-1":10,"top-2":15},
                            {"x":20,"top-1":100,"top-2":35},
                            {"x":30,"top-1":15,"top-2":75},
                            {"x":40,"top-1":50,"top-2":45}];
        $scope.datacolumns=[{"id":"top-1","type":"spline","name":"Top one","color":"black"},
                            {"id":"top-2","type":"spline","name":"Top two","color":"blue"}];
        $scope.datax={"id":"x"};
        
        //Chart 3
        $scope.chart4datapoints=[];
            $scope.chart4datacolumns=[{"id":"top-1","type":"line","name":"Top one","color":"black"},
                                {"id":"top-2","type":"spline","name":"Top two"}];

            $interval(function(){
                dataService.loadData(function(data){
                    $scope.chart4datapoints.push(data);
                });
            },1000,10);
        
    };
    
    chart4Controller.inject = ['$scope', 'myApp.services', '$interval'];
    
    angular.module('myApp.chart4').controller('Chart4Controller', chart4Controller);
    
})(angular);