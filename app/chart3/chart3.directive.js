(function(angular){
    'use strict';
    
    var tooltipBarChart = function ($parse, d3Service){
        return{
          restrict: "EA",
            template: "<svg width='850' height='200'></svg>",
            link: function(scope, elem, attrs)
            {
                d3Service.d3().then(function(d3){
                    
                    var exp = $parse(attrs.chartData);

                    var chartDataToPlot=exp(scope);
                    
                    var x, y, xAxis, yAxis, tip, margin, width, height;
                    var rawSvg=elem.find('svg');
                    var svg = d3.select(rawSvg[0]);
                    
                    function setParameters (){                    
                        
                        var formatPercent = d3.format(".0%");

                        margin = {top: 40, right: 20, bottom: 30, left: 40};
                        width = 960 - margin.left - margin.right;
                        height = 500 - margin.top - margin.bottom;
                        x = d3.scale.ordinal()
                            .domain(chartDataToPlot.map(function(d) { return d.letter; }))
                            .rangeRoundBands([0, width], .1);

                        y = d3.scale.linear()
                            .domain([0, d3.max(chartDataToPlot, function(d) { return d.frequency; })])
                            .range([height, 0]);

                        xAxis = d3.svg.axis()
                            .scale(x)
                            .orient("bottom");

                        yAxis = d3.svg.axis()
                            .scale(y)
                            .orient("left")
                            .tickFormat(formatPercent);
                        
                        /*tip = d3.tip()
                            .attr('class', 'd3-tip')
                            .offset([-10, 0])
                            .html(function(d) {
                                return "<strong>Frequency:</strong> <span style='color:red'>" + d.frequency + "</span>";
                            })*/
                    }
                    
                    function drawChart() {
                        setParameters();
                        
                        svg.attr("width", width + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom)
                            .append("svg:g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                            
                        //svg.call(tip);
                        
                        svg.append("svg:g")
                                .attr("class", "y axis")
                                .call(yAxis)
                                .append("text")
                                .attr("transform", "rotate(-90)")
                                .attr("y", 6)
                                .attr("dy", ".71em")
                                .style("text-anchor", "end")
                                .text("Frequency");
                                
                        svg.selectAll(".bar")
                                .data(chartDataToPlot)
                                .enter().append("svg:rect")
                                .attr("class", "bar")
                                .attr("x", function(d) { return x(d.letter); })
                                .attr("width", x.rangeBand())
                                .attr("y", function(d) { return y(d.frequency); })
                                .attr("height", function(d) { return height - y(d.frequency); })
                                /*.attr("tooltip-append-to-body", true)
                                .attr("tooltip", function(d){
                                    return d.frequency;
                                })*/
                                .attr("title", function(d){
                                    return  d.frequency;
                                })
                                //.on('mouseover', tip.show)
                                //.on('mouseout', tip.hide)
            
                    }
                    
                    drawChart();
                });
            }  
        };
    };
    
    angular.module('myApp.chart3').directive('tooltipBarChart', tooltipBarChart);
    
})(angular);