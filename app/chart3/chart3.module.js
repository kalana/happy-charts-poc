'use strict';

angular.module('myApp.chart3', ['ngRoute', 'd3'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/chart3', {
    templateUrl: 'chart3/chart3.html',
    controller: 'Chart3Controller',
    controllerAs: 'Chart3Controller'
  });
}])