(function(angular){
    'use strict';
    
    var chart3Controller = function ($scope) {
        $scope.chartData = [
            {letter: 'A', frequency: .08167},
            {letter: 'B', frequency: .01492},
            {letter: 'D', frequency: .02780},
            {letter: 'E', frequency: .04253},
            {letter: 'F', frequency: .12702},
            {letter: 'G', frequency: .02288},
            {letter: 'H', frequency: .02022},
            {letter: 'I', frequency: .06094},
            {letter: 'J', frequency: .06973},
            {letter: 'K', frequency: .00153},
            
        ];
        
    };
    
    chart3Controller.inject = ['$scope'];
    
    angular.module('myApp.chart3').controller('Chart3Controller', chart3Controller);
    
})(angular);