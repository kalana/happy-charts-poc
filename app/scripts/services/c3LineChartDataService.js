var services = angular.module('myApp.services', []);
services.factory('dataService', function() {
    function DataService() {
        var maxNumber = 100;
 
        // API methods
        this.loadData = function(data) {
            data({"x":new Date(),"top-1":randomNumber(),"top-2":randomNumber()});
        };
 
        function randomNumber() {
            return Math.floor((Math.random() * maxNumber) + 1);
        }
    }
    return new DataService();
});