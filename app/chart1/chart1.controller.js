(function (angular) {
    
    'use strict';

    var chart1Controller = function ($scope, $interval){
        $scope.chartData = [
        {hour: 1,sales: 54},
        {hour: 2,sales: 66},
        {hour: 3,sales: 77},
        {hour: 4,sales: 70},
        {hour: 5,sales: 60},
        {hour: 6,sales: 63},
        {hour: 7,sales: 55},
        {hour: 8,sales: 47},
        {hour: 9,sales: 55},
        {hour: 10,sales: 30}
    ];
    
    $interval(function(){
            var hour=$scope.chartData.length+1;
            var sales= Math.round(Math.random() * 100);
            $scope.chartData.push({hour: hour, sales:sales});
        }, 1000, 15);
    };
    
    chart1Controller.inject = ['$scope','$interval'];
    
    angular.module('myApp.chart1').controller('Chart1Controller', chart1Controller);
    
})(angular);