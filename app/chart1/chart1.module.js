'use strict';

angular.module('myApp.chart1', ['ngRoute', 'd3'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/chart1', {
    templateUrl: 'chart1/chart1.html',
    controller: 'Chart1Controller',
    controllerAs: 'Chart1Controller'
  });
}])