'use strict';

angular.module('myApp.c3simple', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/c3simple', {
    templateUrl: 'c3simple/c3simple.html',
    controller: 'C3SimpleCtrl'
  });
}])

.controller('C3SimpleCtrl', [function() {
	var chart = c3.generate({
    bindto: '#chart',
    data: {
      columns: [
        ['data1', 30, 200, 100, 400, 150, 250],
        ['data2', 50, 20, 10, 40, 15, 25]
      ]
    }
});
}]);