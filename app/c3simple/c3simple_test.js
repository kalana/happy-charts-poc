'use strict';

describe('myApp.c3simple module', function() {

  beforeEach(module('myApp.c3simple'));

  describe('c3simple controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var C3SimpleCtrl = $controller('C3SimpleCtrl');
      expect(C3SimpleCtrl).toBeDefined();
    }));

  });
});